#include "BoxClass.h"

using namespace ClassBox;

BoxClassException::BoxClassException(const string &message) : message(message) {}

BoxClass::BoxClass(int length, int width, int height, double weight, int value)
{
	if (length <= 0 || width <= 0 || height <= 0 || weight <= 0 || value <= 0)
	{
		throw BoxClassException("������������ ����!");
	}
	this->length = length;
	this->width = width;
	this->height = height;
	this->weight = weight;
	this->value = value;
}

BoxClass::BoxClass() {}

bool BoxClass::operator ==(const BoxClass &box)
{
	return this->length == box.length && this->width == box.width && this->height == box.height &&
		this->weight == box.weight && this->value == box.value;
}

int BoxClass::getLength()
{
	return length;
}

void BoxClass::setLength(int length)
{
	if (length <= 0) { throw BoxClassException("������������ ����!"); }
	this->length = length;
}

int BoxClass::getWidth()
{
	return width;
}

void BoxClass::setWidth(int width)
{
	if (width <= 0) { throw BoxClassException("������������ ����!"); }
	this->width = width;
}

int BoxClass::getHeight()
{
	return height;
}

void BoxClass::setHeight(int height)
{
	if (height <= 0) { throw BoxClassException("������������ ����!"); }
	this->height = height;
}

double BoxClass::getWeight()
{
	return weight;
}

void BoxClass::setWeight(double weight)
{
	if (weight < 0) { throw BoxClassException("������������ ����!"); }
	this->weight = weight;
}

int BoxClass::getValue()
{
	return value;
}

void BoxClass::setValue(int value)
{
	if (value <= 0) { throw BoxClassException("������������ ����!"); }
	this->value = value;
}

