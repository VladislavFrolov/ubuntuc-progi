#include <iostream> 

using namespace std;

namespace ClassBox {
	

	struct BoxClassException {
		string message;
		BoxClassException(const string& message);
	};

	class BoxClass {

	private:

		int length;
		int width;
		int height;
		double weight;
		int value;

	public:

		BoxClass();
		BoxClass(int length, int width, int height, double weight, int value);

		bool operator == (const BoxClass &box);

		int getLength();
		void setLength(int length);
		int getWidth();
		void setWidth(int width);
		int getHeight();
		void setHeight(int height);
		double getWeight();
		void setWeight(double weight);
		int getValue();
		void setValue(int value);

		friend istream& operator >> (istream &in, BoxClass &box)
		{
			in >> box.length >> box.width >> box.height >> box.weight >> box.value;
			return in;
		}

		friend ostream& operator << (ostream &out, const BoxClass &box)
		{
			out << '(' << box.length << ',' << box.width << ',' << box.height << ',' << box.weight << ',' << box.value << ')' << endl;
			return out;
		}
	};
}

