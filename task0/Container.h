#include <iostream> 
#include <vector> 
#include <fstream>
#include "BoxClass.h"

using namespace ClassBox;

namespace ContainerClass {

	struct ContainerException {
		string message;
		ContainerException(const string &message);
	};

	class Container {

	private:

		vector<BoxClass> boxes;
		int length;
		int width;
		int height;
		double limitedWeight;

	public:

		Container();
		Container(int Length, int Width, int Height, double LimitedWeight, vector<BoxClass> ivec);

		int getSize();
		double sumWeight();
		int sumValue();
		BoxClass getByIndex(int index);
		int push(BoxClass box);
		void pop(int index);

		~Container();

		BoxClass& operator[] (int index);

		friend istream& operator >> (istream &in, Container &container)
		{
			int N = container.boxes.size();
			container.boxes.clear();

			for (int i = 0; i < N; i++) {
				BoxClass box(1, 1, 1, 1, 1);
				in >> box;
				container.boxes.push_back(box);
			}

			in >> container.length >> container.width >> container.height >> container.limitedWeight;
			return in;
		}

		friend ostream& operator << (ostream &out, const Container &container)
		{
			out << "( [ " << container.length << ", " << container.width << ", " << container.height << ", " << container.limitedWeight;

			for (BoxClass box : container.boxes)
			{
				out << box << " ";
			}

			out << " ], "  << " )";
			return out;
		}

	};
}