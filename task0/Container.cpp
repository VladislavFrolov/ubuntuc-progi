#include "Container.h"

using namespace ContainerClass;

ContainerException::ContainerException(const string &message) : message(message) {}


Container::Container(int Length, int Width, int Height, double BoundWeight, vector<BoxClass> ivec)
{
	if (Length <= 0 || Width <= 0 || Height <= 0 || BoundWeight <= 0) { throw ContainerException("������������ ����!"); }

	boxes = ivec;
	length = Length;
	width = Width;
	height = Height;
	limitedWeight = BoundWeight;
}

Container::Container() {}

//���������� ������� � ����������
int Container::getSize()
{
	return boxes.size();
}

//��������� ��� ����������� ����������
double Container::sumWeight()
{
	double sum = 0;

	for (BoxClass box : boxes)
	{
		sum += box.getWeight();
	}

	return sum;
}

//��������� ��������� ����������� 
int Container::sumValue()
{
	int sum = 0;

	for (BoxClass box : boxes)
	{
		sum += box.getValue();
	}
	return sum;
}

//��������� ������� �� �������
BoxClass Container::getByIndex(int index)
{
	if (index < 0 || index > getSize()) { throw BoxClassException("������������ ����!"); }

	return boxes.operator[](index);
}

//���������� ������� � ���������
int Container::push(BoxClass box)
{
	if (sumWeight()  > limitedWeight) { throw ContainerException("������ �������� �������!"); }

	boxes.push_back(box);

	return boxes.size() - 1;
}

//�������� �������
void Container::pop(int index)
{
	if (index < 0 || index > getSize()) { throw BoxClassException("������������ ����!"); }

	boxes.erase(boxes.begin() + index);
}

Container::~Container() {}

BoxClass & Container::operator[](int index)
{
	if (index < 0 || index > getSize()) { throw BoxClassException("������������ ����!"); }

	return boxes.operator[](index);
}
