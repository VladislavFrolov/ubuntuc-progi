cmake_minimum_required(VERSION 3.14)
project(task6)

set(CMAKE_CXX_STANDARD 14)

add_executable(task6 main.cpp BinarySearchTree.cpp BinarySearchTree.h)