#include <iostream>
#include "BinarySearchTree.h"

using namespace std;

int main() {
    auto *searchTree = new BinarySearchTree();
    searchTree->add("Volodya");
    searchTree->add("Dmitriy");
    searchTree->add("Alexey");
    searchTree->add("Andrey");
    searchTree->add("Alena");
    searchTree->add("Alena");
    cout << *searchTree;
    cout << searchTree->getSize() << endl;
    cout << "Alena's frequency: " << searchTree->find("Alena") << endl;

    searchTree->remove("Alena", searchTree->root);
    cout << "\nAfter remove 'Alena'" << endl;
    cout << *searchTree;
    cout << searchTree->getSize() << endl;

    searchTree->remove("Dmitriy", searchTree->root);
    searchTree->remove("Alexey", searchTree->root);
    searchTree->remove("Andrey", searchTree->root);
    searchTree->remove("Volodya", searchTree->root);
    searchTree->remove("Alena", searchTree->root);
    cout << "\nAfter remove 'Dmitriy'" << endl;
    cout << *searchTree;
    cout << searchTree->getSize() << endl;

    searchTree->add("Alena");
    cout << "\nAfter add 'Alena'" << endl;
    cout << *searchTree;
    cout << searchTree->getSize() << endl;
    return 0;
}