
#include "BinarySearchTree.h"
#include <iostream>
using namespace std;

BinarySearchTreeNode::BinarySearchTreeNode(const string &value, int frequency, BinarySearchTreeNode *left,
                                           BinarySearchTreeNode *right) : value(value), frequency(frequency),
                                                                          left(left), right(right) {}

BinarySearchTreeNode::BinarySearchTreeNode(const string &value, BinarySearchTreeNode *left, BinarySearchTreeNode *right)
        : BinarySearchTreeNode(value, 1, left, right) {}

BinarySearchTreeNode::BinarySearchTreeNode() = default;

const string &BinarySearchTreeNode::getValue() const {
    return value;
}

void BinarySearchTreeNode::setValue(const string &value) {
    BinarySearchTreeNode::value = value;
}

int BinarySearchTreeNode::getFrequency() const {
    return frequency;
}

void BinarySearchTreeNode::setFrequency(int frequency) {
    BinarySearchTreeNode::frequency = frequency;
}

BinarySearchTreeNode *BinarySearchTreeNode::getLeft() const {
    return left;
}

void BinarySearchTreeNode::setLeft(BinarySearchTreeNode *left) {
    BinarySearchTreeNode::left = left;
}

BinarySearchTreeNode *BinarySearchTreeNode::getRight() const {
    return right;
}

void BinarySearchTreeNode::setRight(BinarySearchTreeNode *right) {
    BinarySearchTreeNode::right = right;
}



BinarySearchTree::BinarySearchTree(BinarySearchTreeNode *root) : root(root) {}

BinarySearchTree::~BinarySearchTree() {
    clear(root);
}

void BinarySearchTree::clear(BinarySearchTreeNode *deletedNode) {
    if (deletedNode == nullptr)
        return;

    clear(deletedNode->getLeft());
    clear(deletedNode->getRight());
    delete deletedNode;
}

int BinarySearchTree::getSize() const {
    return size;
}

BinarySearchTree &BinarySearchTree::operator=(const BinarySearchTree &binaryTree) {
    this->clear(this->root);
    this->root = copy(binaryTree.root);
    return *this;
}

BinarySearchTree &BinarySearchTree::operator=(BinarySearchTree &&binaryTree) noexcept {
    this->clear(this->root);
    this->root = new BinarySearchTreeNode(binaryTree.root->getValue(), binaryTree.root->getFrequency(),
                                          binaryTree.root->getLeft(), binaryTree.root->getRight());
    return *this;
}

BinarySearchTreeNode *BinarySearchTree::copy(BinarySearchTreeNode *copyableNode) {
    if (copyableNode == nullptr) {
        return nullptr;
    } else {
        auto *newNode = new BinarySearchTreeNode();
        newNode->setValue(copyableNode->getValue());
        newNode->setFrequency(copyableNode->getFrequency());
        newNode->setLeft(copy(copyableNode->getLeft()));
        newNode->setRight(copy(copyableNode->getRight()));
        return newNode;
    }
}

int BinarySearchTree::find(const string &word) const {
    BinarySearchTreeNode *current = root;
    while(current != nullptr && word != current->getValue()) {
        if(word < current->getValue()) {
            current = current->getLeft();
        } else {
            current = current->getRight();
        }
    }
    return current == nullptr ? 0 : current->getFrequency();
}

void BinarySearchTree::add(const string &word) {
    BinarySearchTreeNode *current = root;
    BinarySearchTreeNode *parent = root;
    bool isLeftSide = false;

    while (current != nullptr && word != current->getValue()) {
        parent = current;
        if(word < current->getValue()) {
            isLeftSide = true;
            current = current->getLeft();
        } else {
            isLeftSide = false;
            current = current->getRight();
        }
    }
    if(current == nullptr) {
        auto *newNode = new BinarySearchTreeNode(word, 1, nullptr, nullptr);
        if(parent == nullptr) {
            root = newNode;
        } else {
            if(isLeftSide) {
                parent->setLeft(newNode);
            } else {
                parent->setRight(newNode);
            }
        }
    } else {
        current->setFrequency(current->getFrequency() + 1);
    }
    size++;
}


void BinarySearchTree::remove(const string &word, BinarySearchTreeNode *&root) {
    if(root == nullptr) {
        return;
    }
    if(word < root->getValue()) {
        remove(word, root->left);
    } else if(word > root->getValue()) {
        remove(word, root->right);
    } else {
        root->frequency = root->frequency - 1;
        size--;
        if(root->getFrequency() <= 0) {
            BinarySearchTreeNode *tmp = root;
            if(root->right == nullptr) {
                root = root->left;
            } else if(root->left == nullptr) {
                root = root->right;
            } else {
                smartDelete(tmp, root->left);
            }
            delete tmp;
        }
    }

}

void BinarySearchTree::smartDelete(BinarySearchTreeNode *&root, BinarySearchTreeNode *&maxLeft) {
    if(maxLeft->right != nullptr) {
        smartDelete(root, maxLeft->right);
    } else {
        root->value = maxLeft->value;
        root->frequency = maxLeft->frequency;
        root = maxLeft;
        maxLeft = maxLeft->left;
    }
}

ostream &operator<<(ostream &os, const BinarySearchTree &tree) {
    string output;
    tree.print(tree.root, output);
    os << "Tree: \n" << output << endl;
    return os;
}

void BinarySearchTree::print(BinarySearchTreeNode *root, string &output) const {
    if(root != nullptr) {
        print(root->getLeft(), output);
        output.append(root->value).append(" - ").append(to_string(root->frequency)).append("\n");
        print(root->getRight(), output);
    }
}


BinarySearchTree::BinarySearchTree(){ root = nullptr; }




