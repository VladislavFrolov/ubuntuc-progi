//
// Created by Ирина on 17.06.2019.
//

#ifndef TASK6_BINARYSEARCHTREE_H
#define TASK6_BINARYSEARCHTREE_H

#include <string>
#include <ostream>

using namespace std;

class BinarySearchTree;
class BinarySearchTreeNode {
private:
    string value;
    int frequency;
    BinarySearchTreeNode *left;
    BinarySearchTreeNode *right;

public:
    BinarySearchTreeNode(const string &value, int frequency, BinarySearchTreeNode *left, BinarySearchTreeNode *right);

    BinarySearchTreeNode(const string &value, BinarySearchTreeNode *left, BinarySearchTreeNode *right);

    BinarySearchTreeNode();

    const string &getValue() const;

    void setValue(const string &value);

    int getFrequency() const;

    void setFrequency(int frequency);

    BinarySearchTreeNode *getLeft() const;

    void setLeft(BinarySearchTreeNode *left);

    BinarySearchTreeNode *getRight() const;

    void setRight(BinarySearchTreeNode *right);

    friend BinarySearchTree;
};

class BinarySearchTree {
private:
    int size=0;

    void clear(BinarySearchTreeNode *deletedNode);

    BinarySearchTreeNode *copy(BinarySearchTreeNode *copyableNode);

    void smartDelete(BinarySearchTreeNode *&root, BinarySearchTreeNode *&maxLeft);

    void print(BinarySearchTreeNode *root, string &output) const;
public:
    BinarySearchTreeNode* root;

    BinarySearchTree(BinarySearchTreeNode *root);

    BinarySearchTree();

    virtual ~BinarySearchTree();

    int getSize() const;

    BinarySearchTree &operator=(const BinarySearchTree &binaryTree);

    BinarySearchTree &operator=(BinarySearchTree &&binaryTree) noexcept;

    int find(const string &word) const;

    virtual void add(const string &word);

    virtual void remove(const string &word, BinarySearchTreeNode *&root);

    friend ostream &operator<<(ostream &os, const BinarySearchTree &tree);

};

#endif //TASK6_BINARYSEARCHTREE_H
