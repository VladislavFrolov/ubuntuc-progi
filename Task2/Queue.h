#include "iostream"
#include "conio.h"
#include "locale.h"
#include "fstream"

using namespace std;

struct QueueException {
	string message;
	QueueException(const string message) { this->message = message; }
};

	class Queue
	{
	private:
		int size;
		int *queue;
		int head, tail;
		bool empty;
	public:
		Queue();
		Queue(int size);
		~Queue();
		void addElem(int elem);
		int getElem();
		int seeHead();
		bool isEmpty();
		void makeEmpty();
		int getSize();
		friend class Iterator;
	};
