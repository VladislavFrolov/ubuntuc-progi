#include "iostream"
#include "conio.h"
#include "locale.h"
#include "fstream"

using namespace std;

struct SizeException {
	string message;
	SizeException(const string message) { this->message = message; }
};
struct IndexException {
	string message;
	IndexException(const string message) { this->message = message; }
};

class DynamArray {
private:
	int *arr;
	int size;
	
public:

	DynamArray() 
	{
		arr = new int[10];
		size = 10;
	}

	DynamArray(int size) 
	{
		arr = new int[size];
		this->size = size;
	}

	DynamArray(int size, int n) 
	{
		arr = new int[size];
		for (int i = 0; i < size; i++) 
		{
			arr[i] = n;
		}
		this->size = size;
	}

	//����������� �����������
	DynamArray(const DynamArray &obj) 
	{
		size = obj.size;
		arr = new int[size];
		for (int i = 0; i < size; i++) 
		{
			arr[i] = obj.arr[i];
		}
	}

	//����������� ����������� 
	DynamArray(DynamArray &&obj)
	{
		size = obj.size;
		arr = obj.arr;
		obj.size = 0;
		obj.arr = nullptr;
	}

	~DynamArray() 
	{
		delete[] arr;
		size = NULL;
	}

	int getSize() 
	{
		return size;
	}

	int getElem(int i) 
	{
		return arr[i];
	}

	int operator [](int i) 
	{
		if (i <0 || i >= this->size) 
		{
			throw IndexException("����� �� �������!");
		}
		return arr[i];
	}

	DynamArray &operator =(const DynamArray &obj) 
	{
		if (this == &obj) 
		{ 
			return *this; 
		}
		delete[] this->arr;
		this->arr = new int[obj.size];
		for (int i = 0; i < obj.size; i++) 
		{
			arr[i] = obj.arr[i];
		}
		this->size = obj.size;
		return *this;
	}

	//�������� �����������
	DynamArray &operator =(DynamArray &&obj) 
	{
		if (this == &obj)
		{
			return *this;
		}
		delete[] this->arr;
		this->arr = new int[obj.size];
		this->size = obj.size;
		arr = obj.arr;
		obj.size = 0;
		obj.arr = nullptr;
		return *this;
	}

	void resize(int newSize) 
	{
		if (newSize < 0) 
		{
			throw SizeException("������ �� ����� ���� ������ ����!");
		}

		if (newSize > this->size) 
		{
			int *buf = new int[newSize];
			for (int i = 0; i < newSize; i++) 
			{
				if (i < size) 
				{
					buf[i] = this->arr[i];
				}
				else 
				{
					buf[i] = 0;
				}
			}
			size = newSize;
			delete[]arr;
			this->arr = buf;
		}
		else 
		{  
			int *buf = new int[newSize];
			for (int i = 0; i < newSize; i++)
			{
					buf[i] = this->arr[i];
			}
			size = newSize;
			delete[]arr;
			this->arr = buf;
		}
	}

	bool operator ==(const DynamArray &obj) 
	{
		if (obj.size  != this->size)
		{
			throw SizeException("������� ������� �������!");
		}
		else 
		{
			for (int i = 0; i < this->size; i++)
			{
				if (obj.arr[i]  != this->arr[i])
				{
					return false;
				}
			}
			return true;
		}
	}

	bool operator !=(const DynamArray &obj) 
	{
		if (this->size != obj.size) 
		{
			throw SizeException("������� ������� �������!");
		}
		else 
		{
			for (int i = 0; i < this->size; i++)
			{
				if (this->arr[i] != obj.arr[i])
				{
					return true;
				}
			}
			return true;
		}
	}

	bool operator <(const DynamArray &obj) 
	{
		int n = this->size;
		int i = 0;

		if (n < obj.size)
		{
			n = obj.size;
		}

		while (i < n && this->arr[i] <= obj.arr[i])
		{
			
			if (this->arr[i] < obj.arr[i])
			{
				return true;
			}

			i++;
		}
		return false;
	}

	bool operator <=(const DynamArray &obj) 
	{
		int n = this->size, i = 0;

		if (n < obj.size)
		{
			n = obj.size;
		}

		while (i < n && this->arr[i] <= obj.arr[i]) 
		{
			if (this->arr[i] < obj.arr[i]) 
			{
				return true;
			}

			i++;
		}
		return false;
	}

	bool operator >(const DynamArray &obj) 
	{
		int n = obj.size, i = 0;

		if (n < this->size) 
		{
			n = this->size;
		}

		while (i < n && this->arr[i] >= obj.arr[i]) 
		{
			if (this->arr[i] > obj.arr[i]) 
			{
				return true;
			}

			i++;
		}
		return false;
	}

	bool operator >=(const DynamArray &obj) 
	{
		int n = obj.size, i = 0;

		if (n < this->size) 
		{
			n = this->size;
		}

		while (i < n && this->arr[i] >= obj.arr[i]) 
		{
			if (this->arr[i] > obj.arr[i]) 
			{
				return true;
			}

			i++;
		}
		return false;
	}

	DynamArray &operator +(const DynamArray &obj) 
	{
		int newSize = this->size + obj.size;
		DynamArray *sum = new DynamArray(newSize);
		for (int i = 0; i < this->size; i++) {
			sum->arr[i] = this->arr[i];
		}
		for (int j = size; j < newSize; j++) {
			sum->arr[j] = obj.arr[j - size];
		}
		return *sum;
	}

	friend istream &operator>>(istream &in, DynamArray &obj) 
	{
		int newSize;
		in >> newSize;
		obj.resize(newSize);
		obj.size = newSize;
		for (int i = 0; i < obj.size; i++) 
		{
			in >> obj.arr[i];
		}
		return in;
	}

	friend ostream &operator<<(ostream &out, const DynamArray &obj) 
	{
		out << "������ = " << obj.size <<  " ������ = [ ";
		for (int i = 0; i < obj.size; i++) 
		{
			out << obj.arr[i] << ' ';
		}
		out << "]" << endl;
		return out;
	}
};

int main() {

	 setlocale(LC_ALL, "Rus");

	 ifstream in("1.txt");
	 ofstream out("2.txt");

	 int size, n;

	 in >> size;
	 in >> n;
	 DynamArray a (size, n);
	
	 in >> size;
	 in >> n;
	 DynamArray b(size, n);

	
	 out << a << endl;
	 out << b << endl;
	
	 out << "�������� ���������� < " << (a < b) << endl;
	 out << "�������� ���������� <= " << (a <= b) << endl;
	 out << "�������� ���������� > " << (a > b) << endl;
	 out << "�������� ���������� >= " << (a >= b) << endl;
	 out << "�������� ���������� + " << (a + b) << endl;

	 try
	 {
		 b.resize(3);
		 out << "��������� ������� " << b << endl;
	 }
	 catch (SizeException e)
	 {
		 out << "����� ������ 0!" << endl;
	 }                                                                                                                                                                                                                                                                                       
	 
	 try
	 {
		 out << "�������� ���������� == " << (a == b) << endl;
	 }
	 catch (SizeException e)
	 {
		 out << "������� ������� �������!" << endl;
	 }

	 try
	 {
		 out << "�������� ���������� != " << (a != b) << endl;
	 }
	 catch (SizeException e)
	 {
		 out << "������� ������� �������!" << endl;
	 }


	 DynamArray c(a);
	 out << "����������� ����������� " << c << endl;
	 out << "1. " << c << "2. " << b << endl;
	 c = b;
	 out << "�������� = " << c << endl;

	 return 0;
}