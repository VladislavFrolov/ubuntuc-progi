//
// Created by profyan on 17.04.19.
//

#ifndef FITH_HASHMAP_H
#define FITH_HASHMAP_H

#include <string>
#include "HashTableException.h"
#include <functional>

using namespace std;

template<typename K, typename V>
class HashTableNode {
private:
    K key;
    V value;
    HashTableNode<K, V> *next;
public:
    HashTableNode(const K &key, const V &value, HashTableNode<K, V> *next) : key(key), value(value), next(next) {

    }

    HashTableNode(const K &key, const V &value) : HashTableNode(key, value, nullptr) {

    }

    const K &getKey() const {
        return key;
    }

    const V &getValue() const {
        return value;
    }

    HashTableNode<K, V> *getNext() const {
        return next;
    }

    void setValue(const V &value) {
        this->value = value;
    }

    void setNext(HashTableNode<K, V> *next) {
        this->next = next;
    }
};

template<typename K, typename V>
class HashTable {

private:
    HashTableNode<K, V> **table;
    int size;
    static const int TABLE_BASE_SIZE = 16;

    unsigned long getKeyHash(const K &key) const {
        hash<K> keyHash;
        return ((unsigned long) keyHash(key)) % TABLE_BASE_SIZE;
    }


public:
    HashTable() {
        table = new HashTableNode<K, V> *[TABLE_BASE_SIZE]();
        size = 0;
    }

    virtual ~HashTable() {
        clear();
        delete[] table;
    }

    virtual const HashTableNode<K, V> *put(const K &key, const V &value) {
        unsigned long keyHash = getKeyHash(key);
        HashTableNode<K, V> *currentNode = table[keyHash];
        HashTableNode<K, V> *prevNode = nullptr;

        while (currentNode != nullptr && key != currentNode->getKey()) {
            prevNode = currentNode;
            currentNode = currentNode->getNext();
        }

        //Key not found
        if (currentNode == nullptr) {

            auto *newNode = new HashTableNode<K, V>(key, value);

            //first node
            if (prevNode == nullptr) {
                table[keyHash] = newNode;
            } else {
                prevNode->setNext(newNode);
            }
            size++;
            return newNode;
        } else {
            currentNode->setValue(value);
            return currentNode;
        }
    }


    virtual const V &get(const K &key) const {
        unsigned long keyHash = getKeyHash(key);
        HashTableNode<K, V> *currentNode = table[keyHash];

        while (currentNode != nullptr && key != currentNode->getKey()) {
            currentNode = currentNode->getNext();
        }

        if (currentNode != nullptr) {
            return currentNode->getValue();
        } else {
            throw HashTableException("No value for this key");
        }
    }

    virtual void remove(const K &key) {
        unsigned long keyHash = getKeyHash(key);
        HashTableNode<K, V> *currentNode = table[keyHash];
        HashTableNode<K, V> *prevNode = nullptr;

        while (currentNode != nullptr && key != currentNode->getKey()) {
            prevNode = currentNode;
            currentNode = currentNode->getNext();
        }

        if (currentNode != nullptr) {
            // if first node
            if (prevNode == nullptr) {
                table[keyHash] = currentNode->getNext();
            } else {
                prevNode->setNext(currentNode->getNext());
            }
            delete currentNode;
            size--;
        } else {
            throw HashTableException("No value for this key");
        }
    }

    virtual void clear() {
        HashTableNode<K, V> *currentNode = nullptr;
        HashTableNode<K, V> *tmpNode = nullptr;

        for (int i = 0; i < TABLE_BASE_SIZE; i++) {
            currentNode = table[i];
            while (currentNode != nullptr) {
                tmpNode = currentNode;
                currentNode = currentNode->getNext();
                delete tmpNode;
            }
            table[i] = nullptr;
        }
    }

    virtual bool isEmpty() {
        return size == 0;
    }

    class HashTableIterator {

    protected:
        HashTable *hashTable;
        int currentIndex;
        const HashTableNode<K, V> *currentHashTableNode;

        friend HashTable;

        explicit HashTableIterator(HashTable *hashTable) : hashTable(hashTable) {
            currentIndex = 0;
            if(hashTable->isEmpty()) {
                throw HashTableException("HashTable is empty");
            }
            while((currentHashTableNode = hashTable->table[currentIndex]) == nullptr) {
                currentIndex++;
            }

        }

    public:
        virtual void next() {
            if ((currentHashTableNode = currentHashTableNode->getNext()) == nullptr) {
                while (currentHashTableNode == nullptr && ++currentIndex != TABLE_BASE_SIZE) {
                    currentHashTableNode = hashTable->table[currentIndex];
                }
            }
        }

        virtual const HashTableNode<K, V> *getEntry() {
            return currentHashTableNode;
        }

        virtual bool isFinished() {
            return currentIndex == TABLE_BASE_SIZE;
        }

    };

    friend HashTableIterator;

    virtual HashTableIterator *iterator() {
        return new HashTableIterator(this);
    }

};




#endif //FITH_HASHMAP_H
