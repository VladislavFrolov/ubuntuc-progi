//
// Created by profyan on 17.04.19.
//

#ifndef FITH_HASHTABLEEXCEPTION_H
#define FITH_HASHTABLEEXCEPTION_H

#include <string>

using namespace std;

struct HashTableException {
private:
    string message;
public:
    const string &getMessage() const {
        return message;
    }

    explicit HashTableException(const string &message) {
        this->message = message;
    };
};

#endif //FITH_HASHTABLEEXCEPTION_H
