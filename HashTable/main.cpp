#include <iostream>
#include <string>
#include "HashTable.h"
#include "LinkedHashTable.h"

using namespace std;

int main() {
    auto *map = new HashTable<int, string>();
    map->put(1, "Hello");
    map->put(2, "World");

    cout << "What in first: " << map->get(1) << endl;
    cout << "What in second: " << map->get(2) << endl;
    cout << endl;

    map->put(1, "By by");
    cout << "What in first: " << map->get(1) << endl;
    cout << "What in second: " << map->get(2) << endl;

    HashTable<int, string>::HashTableIterator* iterator = map->iterator();
    while(!iterator->isFinished()) {
        cout << iterator->getEntry()->getValue() << endl;
        iterator->next();
    }


    auto *linkedMap = new LinkedHashTable<string, string>();
    linkedMap->put("one", "one value");
    linkedMap->put("two", "two value");
    linkedMap->put("three", "free value");
    linkedMap->put("four", "four value");

    cout << endl << "LinkedHashTable:: " << endl;
    cout << "get 1 el: " << linkedMap->get("one") << endl;
    LinkedHashTable<string, string>::LinkedHashTableIterator* linkedIterator = linkedMap->iterator();
    while(!linkedIterator->isFinished()) {
        cout << linkedIterator->getEntry()->getValue() << endl;
        linkedIterator->next();
    }

    linkedMap->remove("two");
    cout << endl << "Remove two:: " << endl;
    delete linkedIterator;
    linkedIterator = linkedMap->iterator();
    while(!linkedIterator->isFinished()) {
        cout << linkedIterator->getEntry()->getValue() << endl;
        linkedIterator->next();
    }
    return 0;
}