//
// Created by profyan on 18.04.19.
//

#ifndef FITH_LINKEDHASHTABLE_H
#define FITH_LINKEDHASHTABLE_H

#include "HashTable.h"

template<typename K, typename V>
class LinkedHashTableNode {
private:
    LinkedHashTableNode<K, V> *before;
    LinkedHashTableNode<K, V> *after;
    const HashTableNode<K, V> *node;

public:

    LinkedHashTableNode(LinkedHashTableNode<K, V> *before, LinkedHashTableNode<K, V> *after,
                        const HashTableNode<K, V> *node) : before(before), after(after), node(node) {}


    LinkedHashTableNode<K, V> *getBefore() const {
        return before;
    }

    void setBefore(LinkedHashTableNode<K, V> *before) {
        LinkedHashTableNode::before = before;
    }

    LinkedHashTableNode<K, V> *getAfter() const {
        return after;
    }

    void setAfter(LinkedHashTableNode<K, V> *after) {
        LinkedHashTableNode::after = after;
    }

    const HashTableNode<K, V> *getNode() const {
        return node;
    }

    void setNode(const HashTableNode<K, V> *node) {
        LinkedHashTableNode::node = node;
    }

};


template<typename K, typename V>
class LinkedHashTable : public HashTable<K, V> {
    LinkedHashTableNode<K, V> *head;

public:
    LinkedHashTable() : HashTable<K, V>() {
        head = new LinkedHashTableNode<K, V>(nullptr, nullptr, nullptr);
        head->setBefore(head);
        head->setAfter(head);
    }

    LinkedHashTableNode<K, V> *getHead() const {
        return head;
    }

    void setHead(const LinkedHashTableNode<K, V> *head) {
        LinkedHashTable::head = head;
    }

    virtual ~LinkedHashTable() {
        clear();
        delete head;
    }

public:

    virtual const HashTableNode<K, V> *put(const K &key, const V &value) {
        const HashTableNode<K, V> *newHashTableNode = HashTable<K, V>::put(key, value);
        auto *current = new LinkedHashTableNode<K, V>(head->getBefore(), head, newHashTableNode);
        head->setBefore(current);
        current->getBefore()->setAfter(current);
        return newHashTableNode;
    }

    virtual const V &get(const K &key) const {
        return HashTable<K, V>::get(key);
    }

    virtual void remove(const K &key) {
        LinkedHashTableNode<K, V> *current = head->getAfter();
        V value = get(key);
        while (current != head && current->getNode()->getValue() != value) {
            current = current->getAfter();
        }
        if (current != head) {
            current->getBefore()->setAfter(current->getAfter());
            current->getAfter()->setBefore(current->getBefore());
            HashTable<K, V>::remove(key);
        } else {
            throw HashTableException("Not found");
        }

    }

    virtual void clear() {
        LinkedHashTableNode<K, V> *current = head->getAfter();
        LinkedHashTableNode<K, V> *toRemove;
        while (current != head) {
            toRemove = current;
            current = current->getAfter();
            delete toRemove;
        }
        HashTable<K, V>::clear();
    }

    class LinkedHashTableIterator : public HashTable<K, V>::HashTableIterator {

    private:
        LinkedHashTable *linkedHashTable;
        const LinkedHashTableNode<K, V> *currentLinkedHashTableNode;

        friend LinkedHashTable;

    public:
        explicit LinkedHashTableIterator(LinkedHashTable *linkedHashTable) :
                HashTable<K, V>::HashTableIterator(linkedHashTable), linkedHashTable(linkedHashTable) {
            currentLinkedHashTableNode = linkedHashTable->head->getAfter();
        }

        void next() {
            currentLinkedHashTableNode = currentLinkedHashTableNode->getAfter();
        }

        const HashTableNode<K, V> *getEntry() {
            return currentLinkedHashTableNode->getNode();
        }

        bool isFinished() {
            return currentLinkedHashTableNode == linkedHashTable->head;
        }

    };

    virtual LinkedHashTableIterator *iterator() {
        return new LinkedHashTableIterator(this);
    }

    friend LinkedHashTableIterator;

};


#endif //FITH_LINKEDHASHTABLE_H
