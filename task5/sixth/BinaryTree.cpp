#include <utility>

//
// Created by profyan on 12.05.19.
//

#include "BinaryTree.h"
#include <iostream>
#include <string>

using namespace std;

BinaryTree::BinaryTree(BinaryTreeNode *root) : root(root) {}

BinaryTree::~BinaryTree() {
    clear(root);
}

BinaryTree &BinaryTree::operator=(const BinaryTree &binaryTree) {
    this->clear(this->root);
    this->root = copy(binaryTree.root);
    return *this;
}

BinaryTree &BinaryTree::operator=(BinaryTree &&binaryTree) noexcept {
    this->clear(this->root);
    this->root = new BinaryTreeNode(binaryTree.root->getValue(), binaryTree.root->getLeft(), binaryTree.root->getRight());
    return *this;
}

void BinaryTree::clear(BinaryTreeNode *deletedNode) {
    if (deletedNode == nullptr)
        return;

    clear(deletedNode->getLeft());
    clear(deletedNode->getRight());
    delete deletedNode;
}

BinaryTreeNode *BinaryTree::copy(BinaryTreeNode *copyableNode) {
    if (copyableNode == nullptr) {
        return nullptr;
    } else {
        auto *newNode = new BinaryTreeNode();
        newNode->setValue(copyableNode->getValue());
        newNode->setLeft(copy(copyableNode->getLeft()));
        newNode->setRight(copy(copyableNode->getRight()));
        return newNode;
    }
}

void BinaryTree::add(int x, vector<unsigned> &array) {
    BinaryTreeNode *current = this->root;
    BinaryTreeNode *prev = this->root;
    unsigned side = 0;

    for (unsigned &it : array) {
        if (current == nullptr) {
            throw "Invalid input sequence";
        }
        prev = current;
        side = it;
        if (it == 0) {
            current = current->getLeft();
        } else {
            current = current->getRight();
        }
    }

    if (current == nullptr) {
        auto *newNode = new BinaryTreeNode(x, nullptr, nullptr);
        if (side == 0) {
            prev->setLeft(newNode);
        } else {
            prev->setRight(newNode);
        }
    } else {
        current->setValue(x);
    }
}

std::ostream &operator<<(std::ostream &os, const BinaryTree &tree) {
    string output;
    tree.print(tree.root, output, 0);
    os << "Tree: \n" << output << endl;
    return os;
}

void BinaryTree::print(BinaryTreeNode *printableNode, string &output, int tabs) const {
    if (printableNode != nullptr) {
        for (int i = 0; i < tabs; i++) {
            output += " ";
        }
        output.append(to_string(printableNode->getValue()));
        output.append("\n");
        print(printableNode->getLeft(), output, tabs + 4);
        print(printableNode->getRight(), output, tabs + 4);
    }
}

BinaryTree::BinaryTree(int rootValue) : BinaryTree(new BinaryTreeNode(rootValue, nullptr, nullptr)) {

}

int BinaryTree::getEvenElementsAmount(BinaryTreeNode *node) {
    int sum = 0;
    if (node != nullptr) {
        if (node->getValue() % 2 == 0) {
            sum++;
        }
        sum += getEvenElementsAmount(node->getLeft());
        sum += getEvenElementsAmount(node->getRight());
    }
    return sum;
}

bool BinaryTree::isAllElementsPositive(BinaryTreeNode *node) {
    if (node != nullptr) {
        return node->getValue() > 0 && getEvenElementsAmount(node->getLeft()) &&
               getEvenElementsAmount(node->getRight());
    }
    return true;
}

BinaryTreeNode *BinaryTree::deleteAllLeafs(BinaryTreeNode *node) {
    if (node->getRight() == nullptr && node->getLeft() == nullptr) {
        delete node;
        return nullptr;
    }
    if (node->getLeft() != nullptr) {
        node->setLeft(deleteAllLeafs(node->getLeft()));
    }
    if (node->getRight() != nullptr) {
        node->setRight(deleteAllLeafs(node->getRight()));
    }
    return node;
}

int BinaryTree::getSum(BinaryTreeNode *node) {
    if (node == nullptr) {
        return 0;
    }
    return node->getValue() + getSum(node->getLeft()) + getSum(node->getRight());
}

int BinaryTree::getAmount(BinaryTreeNode *node) {
    if (node == nullptr) {
        return 0;
    }
    return 1 + getAmount(node->getLeft()) + getAmount(node->getRight());
}

float BinaryTree::getAverage() {
    return (float) getSum(root) / getAmount(root);
}

bool BinaryTree::find(int x, vector<unsigned> &v, BinaryTreeNode *node) {
    if(node != nullptr) {
        if(find(x, v, node->getLeft())) {
            v.push_back(0);
            return true;
        } else if(find(x, v, node->getRight())) {
            v.push_back(1);
            return true;
        }
        if(node->getValue() == x) {
            return true;
        }
    }
    return false;
}

bool BinaryTree::isSearchBinaryTree(BinaryTreeNode *node) {
    if(node != nullptr) {
        if(node->getLeft() != nullptr && node->getLeft()->getValue() > node->getValue()) {
            return false;
        }

        if(node->getRight() != nullptr && node->getRight()->getValue() < node->getValue()) {
            return false;
        }

        if(!(isSearchBinaryTree(node->getLeft()) && isSearchBinaryTree(node->getRight()))) {
            return false;
        }
    }
    return true;
}


BinaryTreeNode::BinaryTreeNode(int value, BinaryTreeNode *left, BinaryTreeNode *right) : value(value), left(left),
                                                                                         right(right) {}

int BinaryTreeNode::getValue() const {
    return value;
}

void BinaryTreeNode::setValue(int value) {
    BinaryTreeNode::value = value;
}

BinaryTreeNode *BinaryTreeNode::getLeft() const {
    return left;
}

void BinaryTreeNode::setLeft(BinaryTreeNode *left) {
    BinaryTreeNode::left = left;
}

BinaryTreeNode *BinaryTreeNode::getRight() const {
    return right;
}

void BinaryTreeNode::setRight(BinaryTreeNode *right) {
    BinaryTreeNode::right = right;
}

BinaryTreeNode::BinaryTreeNode() : BinaryTreeNode(0, nullptr, nullptr) {}
