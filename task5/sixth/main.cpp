#include <iostream>
#include "BinaryTree.h"
using namespace std;

int main() {
    auto *binaryTree = new BinaryTree(50);
    vector<unsigned> v = {0};
    binaryTree->add(20, v);

    v = {1};
    binaryTree->add(70, v);

    v = {0, 0};
    binaryTree->add(10, v);

    v = {0, 1};
    binaryTree->add(33, v);

    v = {1, 0};
    binaryTree->add(60, v);

    v = {1, 1};
    binaryTree->add(100, v);

    cout << *binaryTree;

    cout << "\n\n" << binaryTree->getEvenElementsAmount(binaryTree->root) << endl;
    cout << "\n\n" << binaryTree->isAllElementsPositive(binaryTree->root) << endl;
    //cout << "\n\nBefore deleting leafs: " << *binaryTree << endl;
    //binaryTree->deleteAllLeafs(binaryTree->root);
    //cout << "\n\nAfter deleting leafs: " << *binaryTree << endl;
    cout << "\n\nAmount: " << binaryTree->getAmount(binaryTree->root) << endl;
    cout << "Sum: " << binaryTree->getSum(binaryTree->root) << endl;
    cout << "Average: " << binaryTree->getAverage() << endl;
    cout << "\n\nSearch 5: " << endl;
    vector<unsigned > searchVector;
    binaryTree->find(5, searchVector, binaryTree->root);
    cout << "Path: ";
    for(unsigned &it : searchVector) {
        cout << it << " ";
    }
    cout << endl;
    cout << "\n\nIs tree a Binary Search Tree: " << binaryTree->isSearchBinaryTree(binaryTree->root) << endl;
    return 0;
}