//
// Created by profyan on 12.05.19.
//

#ifndef SIXTH_BINARYTREE_H
#define SIXTH_BINARYTREE_H

#include <vector>
#include <ostream>

using namespace std;

class BinaryTreeNode {
    int value;
    BinaryTreeNode *left;
    BinaryTreeNode *right;

public:
    BinaryTreeNode(int value, BinaryTreeNode *left, BinaryTreeNode *right);

    BinaryTreeNode();

    int getValue() const;

    void setValue(int value);

    BinaryTreeNode *getLeft() const;

    void setLeft(BinaryTreeNode *left);

    BinaryTreeNode *getRight() const;

    void setRight(BinaryTreeNode *right);
};

class BinaryTree {
private:

    void clear(BinaryTreeNode *deletedNode);

    BinaryTreeNode *copy(BinaryTreeNode *copyableNode);

    void print(BinaryTreeNode *printableNode, string &output, int tabs) const;

public:
    explicit BinaryTree(BinaryTreeNode *root);

    explicit BinaryTree(int rootValue);

    virtual ~BinaryTree();

    BinaryTree &operator=(const BinaryTree &binaryTree);

    BinaryTree &operator=(BinaryTree &&binaryTree) noexcept;

    void add(int x, vector<unsigned> &array);

    friend std::ostream &operator<<(std::ostream &os, const BinaryTree &tree);

    int getEvenElementsAmount(BinaryTreeNode *node);

    bool isAllElementsPositive(BinaryTreeNode *node);

    BinaryTreeNode *deleteAllLeafs(BinaryTreeNode *node);

    int getSum(BinaryTreeNode *node);

    int getAmount(BinaryTreeNode *node);

    float getAverage();

    bool isSearchBinaryTree(BinaryTreeNode *node);

    bool find(int x, vector<unsigned> &v, BinaryTreeNode *node);

    BinaryTreeNode *root;
};


#endif //SIXTH_BINARYTREE_H
