//
// Created by profyan on 24.03.19.
//

#ifndef FOUTH_LIST_H
#define FOUTH_LIST_H


#include "Iterator.h"

class List {
protected:
    int size = 0;
public:

    virtual void push(int el, Iterator* iterator) = 0;

    virtual void remove(Iterator* iterator) = 0;

    virtual Iterator* getFirstEntry(int el) = 0;

    virtual void clear() = 0;

    virtual bool isEmpty() = 0;

    virtual int getSize() = 0;

    virtual Iterator* iterator() const = 0;

};


#endif //FOUTH_LIST_H
