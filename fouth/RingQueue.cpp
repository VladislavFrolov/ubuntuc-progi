//
// Created by profyan on 24.03.19.
//

#include "RingQueue.h"

void RingQueue::push(int el, Iterator *it) {
    auto * iterator = dynamic_cast<RingQueueIterator *>(it);
    auto *newNode = new RingQueueNode();
    RingQueueNode *prevNode = iterator->currentNode;
    newNode->value = el;
    newNode->prev = prevNode;
    newNode->next = prevNode->next;
    prevNode->next = newNode;
    size++;
}

void RingQueue::remove(Iterator *it) {
    auto * iterator = dynamic_cast<RingQueueIterator *>(it);
    RingQueueNode *currentNode = iterator->currentNode;
    if (currentNode == head) {
        throw QueueException("Can't remove");
    }
    currentNode->prev->next = currentNode->next;
    currentNode->next->prev = currentNode->prev;
    delete currentNode;
    size--;
}

Iterator *RingQueue::getFirstEntry(int el) {
    RingQueueNode *current = head->next;
    while (current->value != el && current != head) {
        current = current->next;
    }
    if (current == head) {
        throw QueueException("Not found");
    }
    return new RingQueueIterator(current, this);
}

void RingQueue::clear() {
    RingQueueNode *current = head->next;
    while (current != head) {
        head->next = current->next;
        delete current;
        current = head->next;
    }
    head->prev = head;
    size = 0;
}

bool RingQueue::isEmpty() {
    return head->next == head;
}

int RingQueue::getSize() {
    return size;
}

Iterator * RingQueue::iterator() const {
    return new RingQueueIterator(head->next, this);
}

RingQueue::RingQueue() {
    head = new RingQueueNode();
    head->next = head;
    head->prev = head;
    head->value = 0;
    size = 0;
}

RingQueue::RingQueue(RingQueue &queue) {
    auto *iterator = queue.iterator();
    iterator->start();
    while (!iterator->isFinished()) {
        this->push(iterator->get(), iterator);
    }
    cout << "COPY CONSTRUCTOR" << endl;
}

RingQueue::RingQueue(RingQueue &&queue) noexcept {
    cout << "MOVE CONSTRUCTOR" << endl;
    head->next = queue.head->next;
    head->prev = queue.head->prev;
    size = queue.size;
    cout << "MOVE CONSTRUCTOR" << endl;
}

RingQueue &RingQueue::operator=(RingQueue const &queue) {
    clear();
    auto *iterator = queue.iterator();
    iterator->start();
    while (!iterator->isFinished()) {
        this->push(iterator->get(), iterator);
    }
    cout << "COPY OPERATOR" << endl;
    return *this;
}

RingQueue &RingQueue::operator=(RingQueue &&queue) noexcept {
    clear();
    head->next = queue.head->next;
    head->prev = queue.head->prev;
    size = queue.size;
    cout << "MOVE OPERATOR" << endl;
    return *this;
}

QueueException::QueueException(const string &message) : message(message) {}

const string &QueueException::getMessage() const {
    return message;
}


