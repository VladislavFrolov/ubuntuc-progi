//
// Created by profyan on 24.03.19.
//

#ifndef FOUTH_RINGQUEUE_H
#define FOUTH_RINGQUEUE_H


#include <string>
#include "List.h"
#include <iostream>

using namespace std;

struct QueueException {
private:
    string message;
public:
    const string &getMessage() const;

    explicit QueueException(const string &message);
};

struct RingQueueNode {
    RingQueueNode *next;
    RingQueueNode *prev;
    int value;
};

class RingQueue : public List {

public:

    class RingQueueIterator : public Iterator {
    private:
        RingQueueNode *startNode;
        const RingQueue *queue;

    public:
        explicit RingQueueIterator(RingQueueNode *start, const RingQueue *queue) : startNode(start), queue(queue) {}

        void start() {
            currentNode = startNode;
        }

        int get() {
            return currentNode->value;
        }

        void next() {
            currentNode = currentNode->next;
        }

        bool isFinished() {
            return currentNode == queue->head;
        }

        RingQueueNode *currentNode{};
    };

    friend RingQueueIterator;

    RingQueue();

    RingQueue(RingQueue &queue);

    RingQueue(RingQueue &&queue) noexcept;

    RingQueue &operator=(RingQueue const &queue);

    RingQueue &operator=(RingQueue &&queue) noexcept;

    void push(int el, Iterator *iterator);

    void remove(Iterator *iterator);

    Iterator *getFirstEntry(int el);

    void clear();

    bool isEmpty();

    int getSize();

    Iterator *iterator() const;



private:
    RingQueueNode *head{};

};


#endif //FOUTH_RINGQUEUE_H
