//
// Created by profyan on 11.04.19.
//

#ifndef FOUTH_TESTLIST_H
#define FOUTH_TESTLIST_H

#include "List.h"

class TestList : public List {

public:
    TestList() = default;

    void push(int el, Iterator *iterator) override {

    }

    void remove(Iterator *iterator) override {

    }

    Iterator *getFirstEntry(int el) override {
        return nullptr;
    }

    void clear() override {

    }

    bool isEmpty() override {
        return false;
    }

    int getSize() override {
        return 0;
    }

    Iterator *iterator() {
        return nullptr;
    }
};


#endif //FOUTH_TESTLIST_H
