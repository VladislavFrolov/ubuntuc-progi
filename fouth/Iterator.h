//
// Created by profyan on 24.03.19.
//

#ifndef FOUTH_ITERATOR_H
#define FOUTH_ITERATOR_H


class Iterator {

public:
    virtual void start() = 0;

    virtual int get() = 0;

    virtual void next() = 0;

    virtual bool isFinished() = 0;

    virtual ~Iterator() = default;
};


#endif //FOUTH_ITERATOR_H
