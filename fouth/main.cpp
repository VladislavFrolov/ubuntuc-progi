#include <iostream>
#include "List.h"
#include "RingQueue.h"
#include "TestList.h"

using namespace std;


int main() {
    auto * queue = new RingQueue();
    try {
        Iterator* tmpIterator = queue->iterator();
        tmpIterator->start();
        queue->push(3, tmpIterator);
        queue->push(2, tmpIterator);
        queue->push(1, tmpIterator);
        Iterator* it = queue->iterator();
        it->start();
        while(!it->isFinished()) {
            cout << it->get() << endl;
            it->next();
        }

        tmpIterator->next();
        queue->remove(tmpIterator);
        delete it;
        it = queue->iterator();
        it->start();
        cout << endl <<  "Second list: " << endl;
        while(!it->isFinished()) {
            cout << it->get() << endl;
            it->next();
        }

        Iterator* getEntry = queue->getFirstEntry(3);
        getEntry->start();
        cout << endl << "Get first entry of 3: " << getEntry->get() << endl;

        cout << "Size: " << queue->getSize() << endl;
        cout << "IsEmpty: " << queue->isEmpty() << endl;

        cout << endl << "Clearing..." << endl;
        queue->clear();
        cout << "IsEmpty: " << queue->isEmpty() << " | " << "size: " << queue->getSize() << endl;

        cout << endl << "MOVE TEST" << endl;
        RingQueue ringQueue = RingQueue();
        ringQueue = RingQueue();

        cout << endl << "COPY TEST" << endl;
        RingQueue copy1 = RingQueue();
        RingQueue copy2 = RingQueue();
        copy1 = copy2;

        cout << endl << "COPY CONSTRUCTOR TEST" << endl;
        copy1 = RingQueue(copy2);


    } catch(QueueException& ex) {
        cout << ex.getMessage() << endl;
    }

    return 0;
}
